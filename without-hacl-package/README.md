# P-256 Benchmarking

To run, `dune exec ./test.exe -- -q 10`, where 10 stands for the duration (in seconds) that each of the benchmark will be executed for.

I am currently getting a ~6x slowdown for all OCaml configurations vs the ocaml-uecc code:
```
┌────────────────────┬────────────┬─────────┬──────────┬──────────┬────────────┐
│ Name               │   Time/Run │ mWd/Run │ mjWd/Run │ Prom/Run │ Percentage │
├────────────────────┼────────────┼─────────┼──────────┼──────────┼────────────┤
│ p256 uecc          │   604.43us │         │          │          │     16.04% │
│ p256 hacl          │ 3_767.56us │  30.99w │   -0.12w │   -0.12w │    100.00% │
│ p256 hacl ctypes   │ 3_705.31us │         │   -0.12w │   -0.12w │     98.35% │
│ p256 hacl external │ 3_741.95us │         │   -0.12w │   -0.12w │     99.32% │
└────────────────────┴────────────┴─────────┴──────────┴──────────┴────────────┘
```

However, when comparing just the base pure C functions, the slowdown is only about 2x.
