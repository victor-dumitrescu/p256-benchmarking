(* The MIT License (MIT)
 *
 *   Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE. *)

open Core_bench

module Hacl_P256 = Hacl_P256_bindings.Bindings(Hacl_P256_stubs)

let () = Random.self_init ()

let gen len =
    let open Nocrypto.Rng.Generators.Fortuna in
    let g = create () in
    reseed ~g (Cstruct.of_string "bla");
    let b = generate ~g len in
    Cstruct.to_bytes b

let msg =
  let b = gen 256 in
  Bigstring.of_bytes b

(* let (ed25519_pk, ed25519_signature) =
 *   let open Hacl_star in
 *   let sk_len = 32 in
 *   let pk_len = 32 in
 *   let sig_len = 64 in
 *   let sk = gen sk_len in
 *   let pk = Bytes.create pk_len in
 *   Hacl.Ed25519.secret_to_public pk sk ;
 *   let signature = Bytes.create sig_len in
 *   Hacl.Ed25519.sign signature sk (Bigstring.to_bytes msg) ;
 *   (pk, signature)
 *
 * let ed25519_hacl =
 *   let msg = Bigstring.to_bytes msg in
 *   Bench.Test.create ~name:"ed25519 hacl" (fun () ->
 *       try assert (Hacl.Ed25519.verify ed25519_pk msg ed25519_signature)
 *       with _ -> assert false)
 *
 * external ed25519_verify :
 *   Bigstring.t -> Bigstring.t -> Bigstring.t -> bool =
 *   "ml_Hacl_Ed25519_verify" [@@noalloc]
 *
 * let ed25519_hacl_external =
 *   let pk = Bigstring.of_bytes ed25519_pk in
 *   let signature = Bigstring.of_bytes ed25519_signature in
 *   Bench.Test.create ~name:"ed25519 hacl external" (fun () ->
 *       try assert (ed25519_verify pk msg signature)
 *       with _ -> assert false) *)


(* (\* let (p256_pk, p256_signature) = *\)
 * let gen_key_sig () =
 *   let sk_size = 32 in
 *   let rec get_valid_sk () =
 *     let sk = gen sk_size in
 *     if Hacl.P256.valid_sk sk then sk else get_valid_sk ()
 *   in
 *   (\* let get_valid_sk () =
 *    *   let seed = gen sk_size in
 *    *   let sk = Bytes.create sk_size in
 *    *   Hacl.P256.reduction seed sk;
 *    *   sk
 *    * in *\)
 *   let hacl_p256_keypair () =
 *     let pk_size_raw = 64 in
 *     let pk = Bytes.create pk_size_raw in
 *     let sk = get_valid_sk () in
 *     let pk_of_sk sk pk = Hacl.P256.dh_initiator pk sk in
 *     if pk_of_sk sk pk then (sk, pk) else failwith "P256.keypair: failure"
 *   in
 *   let (sk, pk) = hacl_p256_keypair () in
 *   let sig_size = 64 in
 *   let msg = Bigstring.to_bytes msg in
 *   let signature = Bytes.create sig_size in
 *   let k = get_valid_sk () in
 *   assert (Hacl.P256.sign sk msg k signature) ;
 *   (pk, signature) *)

let gen_key_sig () =
  let open Uecc in
  match Uecc.keypair () with
  | Some (Sk sk, Pk pk) -> begin
      match sign (Sk sk) (Bigstring.to_bytes msg) with
      | Some p256_sig ->
        (pk, p256_sig)
      | None -> failwith "bla2"
    end
  | _ -> failwith "bla"


(* Runs the P-256 verification function from ocaml-uecc *)
let p256_uecc =
  let (p256_pk, p256_signature) = gen_key_sig () in
  let open Uecc in
  (* let curve = secp256r1 in *)
  let p256_pk = Bytes.concat Bytes.empty [ Bytes.make 1 '\004'; p256_pk ] in
  let pk =
    pk_of_bytes p256_pk |> function
    | None -> assert false
    | Some v -> v
  in
  (* let signature = Bigstring.of_bytes p256_signature in *)
  let msg = Bigstring.to_bytes msg in
  Bench.Test.create ~name:"p256 uecc" (fun () ->
      assert (Uecc.verify pk ~msg ~signature:p256_signature))

external p256_verify :
  Bigstring.t -> Bigstring.t -> Bigstring.t -> Bigstring.t -> bool =
  "ml_Hacl_P256_ecdsa_verif_without_hash" [@@noalloc]

(* Runs the same underlying C function from hacl-star-raw, but bypasses
   the Ctypes bindings, using the same style as ocaml-uecc *)
let p256_hacl_external =
  let (p256_pk, p256_signature) = gen_key_sig () in
  let r, s = Bytes.sub p256_signature 0 32, Bytes.sub p256_signature 32 32 in
  let b_r = Bigstring.of_bytes r in
  let b_s = Bigstring.of_bytes s in
  let b_p256_pk = Bigstring.of_bytes p256_pk in
  Bench.Test.create ~name:"p256 hacl external" (fun () ->
      assert (p256_verify b_p256_pk msg b_r b_s))

let tests = [
  p256_uecc; (* p256_hacl; p256_hacl_no_check; p256_hacl_ctypes; *) p256_hacl_external;
  (* p256_hacl_decompress; p256_manual_decompress *)
  (* ed25519_sodium; )ed25519_hacl; ed25519_hacl_external *)]


let () = Core.Command.run (Bench.make_command tests)
