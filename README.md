# P-256 Benchmarking

***Note: A more comprehensive benchmarking tool for HACL\* and other cryptography libraries used in Tezos can be found [here](https://gitlab.com/victor-dumitrescu/hacl-benchmarking/).***

To run either benchmark, `dune exec ./test.exe -- -q 10`, where 10 stands for the duration (in seconds) that each of the benchmark will be executed for.
You'll need to have the ocaml-uecc library: https://gitlab.com/tezos/tezos/tree/master/vendors/ocaml-uecc
