(* The MIT License (MIT)
 *
 *   Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE. *)

open Core_bench
open Hacl_star

module Hacl_P256 = Hacl_P256_bindings.Bindings(Hacl_P256_stubs)

let () = Random.self_init ()

let gen len =
  let write buf =
    if Hacl.RandomBuffer.randombytes buf then ()
    else failwith "Error getting random bytes"
  in
  let buf = Bytes.create len in
  write buf ;
  buf

let msg =
  let b = Bytes.create 256 in
  assert (Hacl_star.Hacl.RandomBuffer.randombytes b) ;
  Bigstring.of_bytes b

let (ed25519_pk, ed25519_signature) =
  let open Hacl_star in
  let sk_len = 32 in
  let pk_len = 32 in
  let sig_len = 64 in
  let sk = gen sk_len in
  let pk = Bytes.create pk_len in
  Hacl.Ed25519.secret_to_public pk sk ;
  let signature = Bytes.create sig_len in
  Hacl.Ed25519.sign signature sk (Bigstring.to_bytes msg) ;
  (pk, signature)

(* let ed25519_sodium =
 *   let ed25519_pk = Sodium.Sign.Bytes.to_public_key ed25519_pk in
 *   let ed25519_signature = Sodium.Sign.Bytes.to_signature ed25519_signature in
 *   let msg = Bigstring.to_bytes msg in
 *   Bench.Test.create ~name:"ed25519 sodium" (fun () ->
 *       try Sodium.Sign.Bytes.verify ed25519_pk ed25519_signature msg
 *       with _ -> assert false) *)

let ed25519_hacl =
  let msg = Bigstring.to_bytes msg in
  Bench.Test.create ~name:"ed25519 hacl" (fun () ->
      try assert (Hacl.Ed25519.verify ed25519_pk msg ed25519_signature)
      with _ -> assert false)

external ed25519_verify :
  Bigstring.t -> Bigstring.t -> Bigstring.t -> bool =
  "ml_Hacl_Ed25519_verify" [@@noalloc]

let ed25519_hacl_external =
  let pk = Bigstring.of_bytes ed25519_pk in
  let signature = Bigstring.of_bytes ed25519_signature in
  Bench.Test.create ~name:"ed25519 hacl external" (fun () ->
      try assert (ed25519_verify pk msg signature)
      with _ -> assert false)


(* let (p256_pk, p256_signature) = *)
let gen_key_sig () =
  let open Hacl_star in
  let sk_size = 32 in
  let rec get_valid_sk () =
    let sk = gen sk_size in
    if Hacl.P256.valid_sk sk then sk else get_valid_sk ()
  in
  (* let get_valid_sk () =
   *   let seed = gen sk_size in
   *   let sk = Bytes.create sk_size in
   *   Hacl.P256.reduction seed sk;
   *   sk
   * in *)
  let hacl_p256_keypair () =
    let pk_size_raw = 64 in
    let pk = Bytes.create pk_size_raw in
    let sk = get_valid_sk () in
    let pk_of_sk sk pk = Hacl.P256.dh_initiator pk sk in
    if pk_of_sk sk pk then (sk, pk) else failwith "P256.keypair: failure"
  in
  let (sk, pk) = hacl_p256_keypair () in
  let sig_size = 64 in
  let msg = Bigstring.to_bytes msg in
  let signature = Bytes.create sig_size in
  let k = get_valid_sk () in
  assert (Hacl.P256.sign sk msg k signature) ;
  (pk, signature)

(* Runs the P-256 verification function from ocaml-uecc *)
let p256_uecc =
  let (p256_pk, p256_signature) = gen_key_sig () in
  let open Uecc in
  (* let curve = secp256r1 in *)
  let p256_pk = Bytes.concat Bytes.empty [ Bytes.make 1 '\004'; p256_pk ] in
  let pk =
    pk_of_bytes p256_pk |> function
    | None -> assert false
    | Some v -> v
  in
  (* let signature = Bigstring.of_bytes p256_signature in *)
  let msg = Bigstring.to_bytes msg in
  Bench.Test.create ~name:"p256 uecc" (fun () ->
      assert (Uecc.verify pk ~msg ~signature:p256_signature))

(* Runs the P-256 verfication function from the high-level hacl-star package, as
   was being used in lib_crypto *)
let p256_hacl =
  let (p256_pk, p256_signature) = gen_key_sig () in
  let msg = Bigstring.to_bytes msg in
  Bench.Test.create ~name:"p256 hacl" (fun () ->
      assert (Hacl.P256.verify p256_pk msg p256_signature))

(* let p256_hacl_no_check =
 *   let (p256_pk, p256_signature) = gen_key_sig () in
 *   let msg = Bigstring.to_bytes msg in
 *   Bench.Test.create ~name:"p256 hacl no check" (fun () ->
 *       assert (Hacl.P256.verify_no_check p256_pk msg p256_signature)) *)

(* Runs the P-256 verfication function from the lower-level hacl-star-raw package
   directly, which is what hacl-star does under the hood *)
let p256_hacl_ctypes =
  let (p256_pk, p256_signature) = gen_key_sig () in
  let size_uint32 b = Unsigned.UInt32.of_int (Bytes.length b) in
  let ctypes_buf = Ctypes.ocaml_bytes_start in
  let msg = Bigstring.to_bytes msg in
  let r, s = Bytes.sub p256_signature 0 32, Bytes.sub p256_signature 32 32 in

  let size_msg = size_uint32 msg in
  let b_msg = ctypes_buf msg in
  let b_p256_pk = ctypes_buf p256_pk in
  let b_r = ctypes_buf r in
  let b_s = ctypes_buf s in

  Bench.Test.create ~name:"p256 hacl ctypes" (fun () ->
      assert (Hacl_P256.hacl_P256_ecdsa_verif_without_hash size_msg b_msg b_p256_pk b_r b_s))

external p256_verify :
  Bigstring.t -> Bigstring.t -> Bigstring.t -> Bigstring.t -> bool =
  "ml_Hacl_P256_ecdsa_verif_without_hash" [@@noalloc]

(* Runs the same underlying C function from hacl-star-raw, but bypasses
   the Ctypes bindings, using the same style as ocaml-uecc *)
let p256_hacl_external =
  let (p256_pk, p256_signature) = gen_key_sig () in
  let r, s = Bytes.sub p256_signature 0 32, Bytes.sub p256_signature 32 32 in
  let b_r = Bigstring.of_bytes r in
  let b_s = Bigstring.of_bytes s in
  let b_p256_pk = Bigstring.of_bytes p256_pk in
  Bench.Test.create ~name:"p256 hacl external" (fun () ->
      assert (p256_verify b_p256_pk msg b_r b_s))

let p256_hacl_decompress =
  let (p256_pk, _) = gen_key_sig () in
  let raw_pk = Bytes.create 65 in
  Hacl.P256.compress_n p256_pk raw_pk;
  let decompressed_pk = Bytes.create 64 in
  Bench.Test.create ~name:"p256 hacl decompress_n" (fun () ->
      assert (Hacl.P256.decompress_n raw_pk decompressed_pk))

let p256_manual_decompress =
  let (p256_pk, _) = gen_key_sig () in
  let raw_pk = Bytes.create 65 in
  Hacl.P256.compress_n p256_pk raw_pk;
  let decompressed_pk = Bytes.create 64 in
  let decompress src dst =
    Bytes.blit src 1 dst 0 64
  in
  Bench.Test.create ~name:"p256 manual decompress" (fun () ->
      (decompress raw_pk decompressed_pk))


let tests = [
  p256_uecc; p256_hacl; (* p256_hacl_no_check; *) p256_hacl_ctypes; p256_hacl_external;
  p256_hacl_decompress; p256_manual_decompress
  (* ed25519_sodium; )ed25519_hacl; ed25519_hacl_external *)]


let () = Core.Command.run (Bench.make_command tests)
